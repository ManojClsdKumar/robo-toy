import { StandardInputCommander } from "./commanders/commander";
import { Robot } from "./robot/robot";

const commander = new StandardInputCommander({ maxX: 5, maxY: 5 });
const robot = new Robot(commander);
