import { Robot } from "../robot/robot";
import { MockCommander } from "../commanders/mockCommander";

describe("Robot E2E Test", () => {
  let commander: MockCommander;
  let robot: Robot;
  const evaluator = jest.spyOn(console, "log");

  beforeEach(() => {
    commander = new MockCommander();
    robot = new Robot(commander);
  });

  describe("Placement", () => {
    it("should correctly report after being placed well", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 3, y: 4 } });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 3,4,NORTH");
    });

    it("informs that it can't be placed off-board", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 93, y: 64 } });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith(
        "Can't place at 93,64 as that would be off-board."
      );
    });
  });

  describe("Facing", () => {
    it("should correctly be facing north on start", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,NORTH");
    });

    it("should correctly say EAST after one RIGHT", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,EAST");
    });

    it("should correctly say WEST after one LEFT", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "LEFT" });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,WEST");
    });

    it("should correctly say EAST after 4 right turns", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,EAST");
    });

    it("should be able to turn the robot before placement and have it remember its facing", () => {
      commander.injectCommand({ verb: "LEFT" });
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,WEST");
    });
  });

  describe("Movement", () => {
    it("should be able to move one space forward", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,1,NORTH");
    });

    it("should not allow movement off board", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "LEFT" });
      commander.injectCommand({ verb: "MOVE" });
      expect(evaluator).toHaveBeenCalledWith("Robot cannot move off-board.");
      commander.injectCommand({ verb: "REPORT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,WEST");
    });

    it("should be able to navigate 4 moves up, right, down and left to return to 0,0", () => {
      commander.injectCommand({ verb: "PLACE", value: { x: 0, y: 0 } });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "RIGHT" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "MOVE" });
      commander.injectCommand({ verb: "REPORT" });
      commander.injectCommand({ verb: "RIGHT" });
      expect(evaluator).toHaveBeenCalledWith("Output: 0,0,NORTH");
    });
  });
});
