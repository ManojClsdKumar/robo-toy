import { CommandSet } from "./robot/robot.interfaces";

export interface CommandFormat<P extends CommandSet> {
  "Input Command": CommandSet;
  XYINPUT: P extends "PLACE" ? string : undefined;
}

export const commandStreamQuery = [
  {
    name: "Input Command",
    type: "list",
    choices: ["MOVE", "REPORT", "PLACE", "LEFT", "RIGHT"],
  },
  {
    name: "XYINPUT",
    suffix: "\nInput as x,y eg. 0,4",
    type: "string",
    choices: ["MOVE", "REPORT", "PLACE", "LEFT", "RIGHT", "PLACE"],
    when: (answers: any) => {
      if (answers["Input Command"] === "PLACE") {
        return true;
      }
      return false;
    },
    validate: (answer: string) => {
      const coordsSplit = answer.split(",");
      if (coordsSplit.length !== 2) {
        return "Failed! Ensure format is x,y eg. 3,4";
      }

      if (/^\+?\d+$/.test(coordsSplit[0]) && /^\+?\d+$/.test(coordsSplit[1])) {
        return true;
      }
      return `Failed to parse ${coordsSplit}, ensure x and y are positive whole numbers!`;
    },
  },
];
