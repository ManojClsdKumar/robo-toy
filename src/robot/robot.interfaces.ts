import { Observable } from "rxjs";

export type CommandSet = "PLACE" | "MOVE" | "LEFT" | "RIGHT" | "REPORT";

export interface Command<P extends CommandSet> {
  verb: CommandSet;
  value?: P extends "PLACE" ? { x: number; y: number } : undefined;
}

export type Facing = "NORTH" | "SOUTH" | "EAST" | "WEST";
export interface positionalChange {
  x: number;
  y: number;
}

export interface CommandModule {
  initateCommander: () => Promise<void>;
  getComms: () => Observable<Command<CommandSet>>;
  getBoardSize: () => { maxX: number; maxY: number };
}
