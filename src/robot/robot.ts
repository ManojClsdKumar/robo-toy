import {
  Command,
  CommandModule,
  CommandSet,
  Facing,
  positionalChange,
} from "./robot.interfaces";

export const movementMap: { [key in Facing]: positionalChange } = {
  NORTH: { x: 0, y: 1 },
  SOUTH: { x: 0, y: -1 },
  EAST: { x: 1, y: 1 },
  WEST: { x: -1, y: 1 },
};

export class Robot {
  private position: { x: number; y: number } | undefined;
  private facing: Facing = "NORTH";

  constructor(private commandModule: CommandModule) {
    this.commandModule.initateCommander();
    this.commandModule.getComms().subscribe((command) => {
      this.executeCommand(command);
    });
  }

  private executeCommand(command: Command<CommandSet>) {
    switch (command.verb) {
      case "MOVE":
        this.move();
        break;
      case "LEFT":
      case "RIGHT":
        this.alterFacing(command.verb);
        break;
      case "REPORT":
        this.report();
        break;
      case "PLACE":
        this.place(command.value as { x: number; y: number });
        break;
    }
  }

  private place(position: { x: number; y: number }) {
    if (!this.isOnBoard(position)) {
      console.log(
        `Can't place at ${position.x},${position.y} as that would be off-board.`
      );
      return;
    }

    this.position = position;
  }

  private alterFacing(command: "LEFT" | "RIGHT") {
    const compass: Facing[] = ["NORTH", "EAST", "SOUTH", "WEST"];
    let facingIndex = compass.findIndex(
      (direction) => direction === this.facing
    );
    if (facingIndex === -1) {
      throw new Error(`Robot is facing a non-existent direction!`);
    }

    if (command === "LEFT") {
      facingIndex--;
    }
    if (command === "RIGHT") {
      facingIndex++;
    }

    if (facingIndex === -1) {
      facingIndex = 3;
    }

    if (facingIndex === 4) {
      facingIndex = 0;
    }

    this.facing = compass[facingIndex];
  }

  private move() {
    const requiredMovement = movementMap[this.facing];

    if (!this.isPlaced(this.position)) {
      console.log(`Robot needs to be placed before movement is allowed`);
      return;
    }

    const hypotheticalFuturePosition = {
      x: this.position.x + requiredMovement.x,
      y: this.position.y + requiredMovement.y,
    };

    if (!this.isOnBoard(hypotheticalFuturePosition)) {
      console.log(`Robot cannot move off-board.`);
      return;
    }

    this.position.x = hypotheticalFuturePosition.x;
    this.position.y = hypotheticalFuturePosition.y;
  }

  private report() {
    if (!this.isPlaced(this.position)) {
      console.log("Unplaced");
      return `Unplaced.`;
    }

    const outputString = `Output: ${this.position.x},${this.position.y},${this.facing}`;
    console.log(outputString);
    return outputString;
  }

  private isOnBoard(position: { x: number; y: number }) {
    const limits = this.commandModule.getBoardSize();
    const laterallyPresent = limits.maxX > position.x && position.x >= 0;
    const longitudinallyPresent = limits.maxY > position.y && position.y >= 0;

    if (laterallyPresent && longitudinallyPresent) {
      return true;
    }

    return false;
  }

  private isPlaced(
    position: { x: number; y: number } | undefined
  ): position is { x: number; y: number } {
    if (this.position === undefined) {
      return false;
    }

    return true;
  }
}
