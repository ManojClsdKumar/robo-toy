import { Subject } from "rxjs";
import { Command, CommandModule, CommandSet } from "../robot/robot.interfaces";

export class MockCommander implements CommandModule {
  constructor() {}
  private comms = new Subject<Command<CommandSet>>();
  async initateCommander() {
    console.log("Mock Commander Initiated, ready to insert commands..");
  }

  getComms() {
    return this.comms;
  }

  public injectCommand(command: Command<CommandSet>) {
    this.comms.next(command);
  }

  public getBoardSize() {
    return { maxX: 5, maxY: 5 };
  }
}
