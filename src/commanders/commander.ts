import asker from "inquirer";
import { sleep, staticLog } from "manojsutils";
import { CommandFormat, commandStreamQuery } from "../dialog";
import { Subject } from "rxjs";
import { Command, CommandSet, CommandModule } from "../robot/robot.interfaces";

export class StandardInputCommander implements CommandModule {
  private publishCommand = new Subject<Command<CommandSet>>();
  constructor(private boardSize: { maxX: number; maxY: number }) {}

  public async initateCommander() {
    staticLog("Robot Activated...");
    await sleep(1000);
    await this.commandLoop();
    return;
  }

  public getComms() {
    return this.publishCommand;
  }

  public getBoardSize() {
    return this.boardSize;
  }
  private async commandLoop() {
    do {
      const result = await asker.prompt(commandStreamQuery);
      this.publishCommand.next(this.parseCommand(result));
    } while (1);
  }

  private parseCommand(
    rawCommand: CommandFormat<CommandSet>
  ): Command<CommandSet> {
    let command: Command<CommandSet> = { verb: rawCommand["Input Command"] };

    if (rawCommand["Input Command"] === "PLACE") {
      const coords = (rawCommand["XYINPUT"] as string).split(",");
      command.value = { x: parseInt(coords[0]), y: parseInt(coords[1]) };
    }

    return command;
  }
}
