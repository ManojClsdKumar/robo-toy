# ROBOT Example

Navigate the robot by providing commands.
Robot isn't allowed off the board.

Use REPORT to see robots current position/direction.

## Extension:

Code your own command module to stream in inputs from other sources.
